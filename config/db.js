var Bookshelf = require('bookshelf');

exports = module.exports = (function () {
    var db_instance = null;

    var create = function () {
        var Mysql = Bookshelf.initialize({
            client: 'mysql',
            connection: {
                host     : '127.0.0.1',
                user     : 'root',
                password : '123456',
                database : 'skinny_font',
                charset  : 'UTF8_GENERAL_CI'
            }
        });
        return Mysql;
    }
    return {
        connect: function () {
            if(!db_instance){
                db_instance = create();
            }
            return db_instance;
        }
    }
})();
