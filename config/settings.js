var mysql = require('mysql');
exports = module.exports = {
    connection: mysql.createConnection({
        host: '127.0.0.1',
        user: 'root',
        password: '123456',
        database: 'skinny_font'
    }),
    pool: mysql.createPool({
        host: '127.0.0.1',
        user: 'root',
        password: '123456',
        database: 'skinny_font'
    }),
    mailConfig: {
        host : '127.0.0.1',
        port : 3000,
        path : '/mail/send',
        method : 'POST',
        encoding : 'utf-8'
    },
    fontGenerateServer: {
        host: '127.0.0.1',
        port: 3100,
        url: 'http://127.0.0.1:3100/font/generateFont',
        convertedFontUrl: 'http://127.0.0.1:3100/convert_font'
    },
    sessionTime: 1000*60*60*24*7
}
