exports = module.exports = {
    logger: require('./logger')(),
    settings: require('./settings'),
    db: require('./db'),
}
