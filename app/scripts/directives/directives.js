'use strict';

angular.module('skinnyFontApp')
    .directive('alertclose', function(){
        return function(scope, element){
            var alert_content = element.parents('.alert');
            element.click(function(event){
                angular.element(alert_content).stop().fadeOut(0);
                event.stopPropagation();
            })
        }
    }).directive('modal', function(){
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                element.find('.modal-close').click(function(){
                    element.removeClass('show');
                })
            }
        }
    }).directive('tlt', function(){
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                element.textillate({
                    loop: true,
                    initialDelay: 500,
                    in: {
                        effect: 'bounce'
                    },
                    out: {
                        effect: 'hinge'
                    }
                });
            }
        }
    }).directive('pin', function(){
        return {
            restrict: "A",
            scope: {
                'pin': '='
            },
            link: function (scope, element, attrs) {
                element.pin({
                    containerSelector: scope.pin
                });
            }
        }
    }).directive('repeatDone', function() {
        return function(scope, element, attrs) {
            element.bind('$destroy', function(event) {
                if (scope.$last) {
                    scope.$eval(attrs.repeatDone);
                }
            });
        }
    }).directive('fontCheck', function(){
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                element.checkbox({
                    onEnable: function(){
                        var data = {
                            operation: '+',
                            fontId: attrs.fontId,
                            fontName: attrs.fontName,
                            fontEnName: attrs.fontEnName,
                            customClass: attrs.fontCustomClass
                        }
                        scope.$emit('updateChooseFont', data);
                        element.find('.font-check-label').text('已选择');
                    },
                    onDisable: function(){
                        var data = {
                            operation: '-',
                            fontId: attrs.fontId
                        }
                        scope.$emit('updateChooseFont', data);
                        element.find('.font-check-label').text('该字体未被选择');
                    }
                });

                var isDefaultChoosen = attrs.fontDefaultConfig || false;
                if(isDefaultChoosen){
                    element.checkbox('enable');
                }
            }
        }
    }).directive('changemenuitem', function () {
        return function postLink(scope, element, attrs) {
            element.on('click', function(){
                element.siblings().removeClass('active');
                element.addClass('active');
            });
        };
    });
