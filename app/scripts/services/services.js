'use strict';

angular.module('skinnyFontApp')
    .service('alertService', function(){
        this.alert = function(speed, alert_type, words){
            if(typeof speed !== 'number' || speed < 0 || speed > 10000){
                speed = 6000;
            }
            if(alert_type !== 'success' && alert_type !== 'failure' && alert_type !== "normal" && alert_type !== "tip"){
                alert_type = 'tip';
            }
            if(typeof words !== 'string' || words.length > 10000){
                words = '提示信息过长';
            }
            angular.element('.alert').stop();
            angular.element('.alert-words').text(words);
            angular.element('.alert').addClass(alert_type).fadeIn(0).delay(speed).fadeOut(0,function(){
                angular.element(this).removeClass(alert_type);
            });
        }
    }).service('authService', function(alertService){
        this.auth = function(data){
            if(data.feedback.type === 'authFailure'){
                if(!angular.element('.overlay-hugeinc').hasClass('open')){
                    angular.element('.overlay-hugeinc').addClass('open');
                }
                alertService.alert(6000, 'failure', data.feedback.message);
                return false;
            }else{
                if(angular.element('.overlay-hugeinc').hasClass('open')){
                    angular.element('.overlay-hugeinc').removeClass('open');
                }
                return true;
            }
        }
    })
