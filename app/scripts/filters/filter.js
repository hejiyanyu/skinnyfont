'use strict';

angular.module('skinnyFontApp')
    .filter('cutString', function(){
        return function(str, len){
            if(str === undefined || len === undefined || str === null || len === null){
                return str;
            }

            var distLength = len;
            var reg = /[\u4e00-\u9fa5]/g;
            var arr = str.match(reg);
            var chStrLength = 0;
            if(arr !== null){
                chStrLength = arr.length;
                distLength = len - arr.length;
                if(distLength < len/2){
                    distLength = len/2;
                }
            }

            if(str.length <= distLength){
                return str;
            }
            else{
                var result = str.substr(0, distLength);
                return result + '...';
            }
        }
    }).filter('price', function(){
        return function(num){
            if(num === undefined || num === null){
                return num;
            }
            if(num > 0){
                return '￥' + num;
            }else{
                return 'FREE';
            }
        }
    })
