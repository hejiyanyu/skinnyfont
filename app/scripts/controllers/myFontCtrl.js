'use strict';

angular.module('skinnyFontApp')
    .controller('myFontCtrl', function ($scope, $q, alertService, $http, settings) {
        $scope.userFontList = [];
        $scope.chooseFontList = [];
        $scope.generateJSCode = '';
        $scope.userPost = {
            host: '',
            user: $scope.currentUser
        }
        var userFontList = [];

        // update chosen font list
        $scope.$on('updateChooseFont', function(event, data){
            if(data.operation === '-'){
                $scope.chooseFontList = _.reject($scope.chooseFontList, function(font){
                    return font.id === data.fontId;
                });
            }else{
                $scope.chooseFontList.push({
                    id: data.fontId,
                    name: data.fontName,
                    class: data.customClass || data.fontEnName
                });
            }
            if(!$scope.$$phase){
                $scope.$digest();
            }
        });

        $scope.getUserFontList = function(){
            var defferred = $q.defer();
            $http({
                method: 'GET',
                url: settings.server + '/user/font/all',
            }).success(function(data){
                if(_.isUndefined(data) || _.isUndefined(data.feedback)){
                    defferred.reject(new Error('no user font data and no feedback'));
                }
                if(data.feedback.type === 'failure'){
                    alertService.alert(6000, 'failure', data.feedback.message)
                    defferred.reject(new Error(data.feedback.message));
                }else{
                    userFontList = data.feedback.userFontList;
                }
                defferred.resolve(userFontList);
            }).error(function(err){
                alertService.alert(6000, 'failure', err);
            })
            return defferred.promise;
        }

        $scope.getUserDefaultConfig = function(){
            var defferred = $q.defer();
            $http({
                method: 'GET',
                url: settings.server + '/user/config',
            }).success(function(data){
                if(_.isUndefined(data) || _.isUndefined(data.feedback)){
                    defferred.reject(new Error('no user font data and no feedback'));
                }
                if(data.feedback.type === 'failure'){
                    alertService.alert(6000, 'failure', data.feedback.message)
                    defferred.reject(new Error(data.feedback.message));
                }else{
                    $scope.userDefaultConfigList = data.feedback.userDefaultConfigList;

                    // refactor the display font list
                    _.each($scope.userDefaultConfigList, function(defaultConfig){
                        _.some(userFontList, function(displayFont){
                            if(defaultConfig.Font_id === displayFont.id){
                                displayFont.defaultConfig = true;
                                displayFont.customClass = defaultConfig.class;
                                return true;
                            }
                            return false;
                        })
                    });
                    $scope.userFontList = userFontList;
                    $scope.userPost.host = $scope.userDefaultConfigList[0].host || '';
                }
                defferred.resolve($scope.userDefaultConfigList);
            }).error(function(err){
                alertService.alert(6000, 'failure', err);
            })
            return defferred.promise;
        }

        $scope.getUserFontList().then($scope.getUserDefaultConfig);

        $scope.generateFont = function(){
            if($scope._validate_font_config_data()){
                return;
            }

            // ace editor
            var editor = ace.edit("code-editor");
            editor.setTheme("ace/theme/monokai");
            editor.getSession().setMode("ace/mode/html");

            $scope.userPost.fontList = $scope.chooseFontList;
            $http({
                method: 'POST',
                url: settings.server + '/font/generateJSCode',
                headers: {'Content-Type': 'application/json;charset=utf-8'},
                data: $scope.userPost
            }).success(function(data){
                if(_.isUndefined(data) || _.isUndefined(data.feedback)){
                    alertService.alert(6000, 'failure', '未获得JSCode');
                }
                if(data.feedback.type === 'failure'){
                    alertService.alert(6000, 'failure', data.feedback.message)
                }else{
                    $scope.generateJSCode = data.code;
                    editor.setValue($scope.generateJSCode);
                }
            }).error(function(err){
                alertService.alert(6000, 'failure', err);
            })
        }

        $scope._validate_font_config_data = function(){
            if($scope.userPost.host.length < 1 || $scope.userPost.host.length > 50){
                alertService.alert(6000, 'failure', '请填写正确的网站域名');
                return true;
            }

            var classList = _.pluck($scope.chooseFontList, 'class');
            var hasNullClass = _.some(classList, function(className){
                return className.length < 1;
            });
            if(hasNullClass){
                alertService.alert(6000, 'failure', '请填写class类名');
                return true;
            }

            if(_.uniq(classList).length < classList.length){
                alertService.alert(6000, 'failure', '请勿填写重复class类名');
                return true;
            }
            return false;
        }
    });
