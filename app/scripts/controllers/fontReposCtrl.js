'use strict';

angular.module('skinnyFontApp')
    .controller('fontReposCtrl', function ($scope, $http, settings, alertService, authService, $q) {
        $scope.fontList = [];
        $scope.userFontList = [];

        $scope.getFontList = function(){
            var defferred = $q.defer();
            $http({
                method: 'GET',
                url: settings.server + '/font/all',
            }).success(function(data){
                if(_.isUndefined(data) || _.isUndefined(data.feedback)){
                    defferred.reject(new Error('no font data and no feedback'));
                }
                if(data.feedback.type === 'failure'){
                    alertService.alert(6000, 'failure', data.feedback.message)
                    defferred.reject(new Error(data.feedback.message));
                }else{
                    $scope.fontList = data.feedback.fontList;
                    _.map($scope.fontList, function(font, index){
                        font.status = 'outOfCart';
                    })
                    defferred.resolve($scope.fontList);
                }
            }).error(function(err){
                alertService.alert(6000, 'failure', err);
            })
            return defferred.promise;
        }

        $scope.getUserFontList = function(){
            var defferred = $q.defer();
            $http({
                method: 'GET',
                url: settings.server + '/user/font/all',
            }).success(function(data){
                if(_.isUndefined(data) || _.isUndefined(data.feedback)){
                    defferred.reject(new Error('no user font data and no feedback'));
                }
                if(data.feedback.type === 'failure'){
                    alertService.alert(6000, 'failure', data.feedback.message)
                    defferred.reject(new Error(data.feedback.message));
                }else{
                    $scope.userFontList = data.feedback.userFontList;
                }
                defferred.resolve($scope.userFontList);
            }).error(function(err){
                alertService.alert(6000, 'failure', err);
            })
            return defferred.promise;
        }

        // handle display font list
        var handleFontList = function(){
            _.map($scope.fontList, function(font){
                _.map($scope.userFontList, function(userFont){
                    if(font.id === userFont.id){
                        font.status = 'purchased';
                    }
                })
                font.status = font.status || 'outOfCart';
                $scope.finalFontList.push(font);
            })
        }

        // update root scope fontlist cache
        var updateRootFontList = function(){
            $scope.$emit('cacheFontList', $scope.finalFontList);
        }

        // get font repository and user's purchased font
        if($scope.finalFontList.length < 1){
            $q.all([$scope.getFontList(), $scope.getUserFontList()]).then(function(values){
                handleFontList();
            });
            updateRootFontList();
        }


        $scope.addFontToCart = function(index){
            var data = {};
            $scope.finalFontList[index].status = "inCart";
            data.font = $scope.finalFontList[index];
            data.operation = '+';
            updateRootFontList();
            $scope.$emit('cart', data);
        }

        $scope.removeFontFromCart = function(index){
            var data = {};
            $scope.finalFontList[index].status = "outOfCart";
            data.font = $scope.finalFontList[index];
            data.operation = '-';
            updateRootFontList();
            $scope.$emit('cart', data);
        }


    });
