'use strict';

angular.module('skinnyFontApp')
    .controller('cartCtrl', function ($scope, alertService, $http, $location, settings) {
        $scope.paySum = 0;
        $scope.initAccordion = function(){
            $('.cart-accordion').accordion();
        }

        // hack for render after ng-repeat
        setTimeout(function(){
            $scope.initAccordion();
        }, 0)

        $scope.calPaySum = function(){
            var sum = _.reduce($scope.fontsInCart, function(memo, num){
                return parseFloat(memo) + parseFloat(num.font_price);
            }, 0);
            return sum;
        }
        $scope.paySum = $scope.calPaySum();

        $scope.pay = function(){
            // pluck the ids from font list in cart
            var fontList = _.pluck($scope.fontsInCart, 'id')
            var sendData = {
                fontList: fontList,
                user: $scope.currentUser
            };

            $http({
                method: 'POST',
                url: settings.server + '/pay',
                headers: {'Content-Type': 'application/json;charset=utf-8'},
                data: sendData
            }).success(function(data){
                if(_.isUndefined(data) || _.isUndefined(data.feedback)){
                    return;
                }
                if(data.feedback.type === 'failure'){
                    alertService.alert(6000, 'failure', data.feedback.message)
                }
                if(data.feedback.type === 'success'){
                    $scope.$emit('removeAllCart');
                    $location.path('/font/myfont')
                    alertService.alert(6000, 'success', data.feedback.message)
                }
            }).error(function(err){
                alertService.alert(6000, 'failure', err);
            })
        }

        $scope.removeFontFromCart = function(font){
            var data = {};
            font.status = "outOfCart";
            data.font = font;
            data.operation = '-';
            $scope.$emit('cart', data);
            $scope.paySum = $scope.calPaySum();
        }
    });
