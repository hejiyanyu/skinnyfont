'use strict';

var app = angular.module('skinnyFontApp', [
    'classy',
    'ngCookies',
    'ngSanitize',
    'ngRoute',
    'uuid4',
    'angular-md5'
]);

app.config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'views/welcome.html'
    }).when('/font/repository', {
        templateUrl: 'views/font-repository.html',
        controller: 'fontReposCtrl'
    }).when('/user/profile', {
        templateUrl: 'views/user-profile.html',
        controller: 'userProfileCtrl'
    }).when('/font/myfont', {
        templateUrl: 'views/my-font.html',
        controller: 'myFontCtrl'
    }).when('/cart', {
        templateUrl: 'views/cart.html',
        controller: 'cartCtrl'
    }).when('/pricing', {
        templateUrl: 'views/pricing.html',
        controller: 'pricingCtrl'
    }).otherwise({
        redirectTo: '/'
    });
});

app.run(function($rootScope, $cookieStore, $http, settings, $timeout, $interval, alertService, md5, authService, $compile, $location){
    var rememberMeCheck = angular.element('#loginModal').find('.checkbox');
    var rememberMeAnswer = angular.element('.remember-me-answer');
    var userProfile = angular.element('.user-profile');
    $rootScope.isLogin = false;
    $rootScope.cartCounts = 0;
    $rootScope.fontsInCart = [];
    $rootScope.finalFontList = [];

    // calculate font in cart
    $rootScope.$on('cart', function(event, data){
        if(data.operation === '+'){
            $rootScope.fontsInCart.push(data.font);
            $rootScope.cartCounts += 1;
        }else{
            // pick font to remove
            $rootScope.fontsInCart = _.reject($rootScope.fontsInCart, function(font){
                return font.id === data.font.id;
            })
            $rootScope.cartCounts -= 1;
        }
    });

    // remove all font from cart
    $rootScope.$on('removeAllCart', function(event){
        $rootScope.fontsInCart = [];
        $rootScope.cartCounts = 0;
    })

    // cache the global font list
    $rootScope.$on('cacheFontList', function(event, data){
        $rootScope.finalFontList = data;
    });

    (function init(){
        angular.element(rememberMeCheck).checkbox('enable', {
            onEnable: function(){
                angular.element(rememberMeAnswer).text('Yes')
            },
            onDisable: function(){
                angular.element(rememberMeAnswer).text('No')
            }
        });
    })();


    $rootScope.showUserProfileMenu = function(){
        angular.element(userProfile).dropdown('toggle');
    }

    $rootScope.logout = function(){
        $http({
            method: 'POST',
            url: settings.server + '/user/logout',
            headers: {'Content-Type': 'application/json;charset=utf-8'},
            data: {}
        }).success(function(data){
            if(_.isUndefined(data) || _.isUndefined(data.feedback)){
                return;
            }
            if(data.feedback.type === 'failure'){
                $rootScope.isLogin = false;
                alertService.alert(6000, 'failure', data.feedback.message)
            }
            if(data.feedback.type === 'success'){
                $rootScope.isLogin = false;
                $rootScope.currentUser = {}
                if(!$rootScope.$$phase){
                    $rootScope.$digest();
                }
                $location.path('/')
                alertService.alert(6000, 'success', "登出成功");
            }
        }).error(function(err){
            alertService.alert(6000, 'failure', err);
        })
    }

    $rootScope.loginMenu = function(){
        angular.element('#loginModal').addClass('show');
    }

    $rootScope.signUpMenu = function(){
        angular.element('#signUpModal').addClass('show');
    }

    $rootScope.login = function(keyCode){
        if(_.isNumber(keyCode) && keyCode !== 13){
            return;
        }

        var username = $rootScope.loginUsername;
        var password = $rootScope.loginPassword;
        var storedSession = angular.element(rememberMeCheck).checkbox('is').enabled();

        if(_.isString(password) && _.isString(username) &&
                password.length > 0 && username.length > 0){
            password = md5.createHash(password);
            var sendData = {
                username: username,
                password: password,
                storedSession: storedSession
            }
            $http({
                method: 'POST',
                url: settings.server + '/user/login',
                headers: {'Content-Type': 'application/json;charset=utf-8'},
                data: sendData
            }).success(function(data){
                if(_.isUndefined(data) || _.isUndefined(data.feedback)){
                    return;
                }
                if(data.feedback.type === 'failure'){
                    $rootScope.isLogin = false;
                    alertService.alert(6000, 'failure', data.feedback.message)
                }
                if(data.feedback.type === 'success'){
                    $rootScope.isLogin = true;
                    $rootScope.currentUser = data.feedback.user
                    if(!$rootScope.$$phase){
                        $rootScope.$digest();
                    }
                    alertService.alert(6000, 'success', "登陆成功");
                    angular.element('#loginModal').removeClass('show');
                }
            }).error(function(err){
                alertService.alert(6000, 'failure', err);
            })
        }else{
            alertService.alert(6000, 'failure', '用户名或密码错误');
        }

    }


    $rootScope.signUp = function(keyCode){
        if(_.isNumber(keyCode) && keyCode !== 13){
            return;
        }

        var username = $rootScope.signUpUsername;
        var password = $rootScope.signUpPassword;

        if(_.isString(password) && _.isString(username) &&
                password.length > 0 && username.length > 0){
            password = md5.createHash(password);
            var sendData = {
                username: username,
                password: password,
            }
            $http({
                method: 'POST',
                url: settings.server + '/user/signup',
                headers: {'Content-Type': 'application/json;charset=utf-8'},
                data: sendData
            }).success(function(data){
                if(_.isUndefined(data) || _.isUndefined(data.feedback)){
                    return;
                }
                if(data.feedback.type === 'failure'){
                    alertService.alert(6000, 'failure', data.feedback.message)
                }
                if(data.feedback.type === 'success'){
                    angular.element('#signUpModal').removeClass('show');
                }
            }).error(function(err){
                console.log(err);
                alertService.alert(6000, 'failure', err);
            })
        }else{
            alertService.alert(6000, 'failure', '用户名或密码不合法');
        }

    }


    // auto login
    $rootScope.autoLogin = function(){
        $http({
            method: 'POST',
            url: settings.server + '/user/auth',
            headers: {'Content-Type': 'application/json;charset=utf-8'},
            data: {}
        }).success(function(data){
            if(_.isUndefined(data) || _.isUndefined(data.feedback)){
                return;
            }
            if(data.feedback.type === 'failure'){
                $rootScope.isLogin = false;
                alertService.alert(6000, 'failure', data.feedback.message);
            }
            if(data.feedback.type === 'success'){
                $rootScope.isLogin = true;
                $rootScope.currentUser = data.feedback.user
                alertService.alert(6000, 'success', "登陆成功");
            }
        }).error(function(err){
            $location.path('/');
            alertService.alert(6000, 'failure', err);
        })
    }
    $rootScope.autoLogin();
});
