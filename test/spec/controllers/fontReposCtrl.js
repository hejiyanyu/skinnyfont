'use strict';

describe('Controller: fontReposCtrl', function () {

  // load the controller's module
  beforeEach(module('skinnyFontApp'));

  var FontReposCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    fontReposCtrl = $controller('fontReposCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
