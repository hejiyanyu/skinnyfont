'use strict';

describe('Controller: cartCtrl', function () {

  // load the controller's module
  beforeEach(module('skinnyFontApp'));

  var cartCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    cartCtrl = $controller('cartCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
