var config = require('../config');
var Mysql = config.db.connect();
var User = require('./userModel');
var Font = require('./fontModel');

exports = module.exports = Mysql.Model.extend({
    tableName: 'User_Font_Relation',

    user: function(){
        return this.belongsTo(User);
    },
    font: function(){
        return this.belongsTo(Font);
    }
});
