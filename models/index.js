exports = module.exports = {
    user: require('./userModel'),
    font: require('./fontModel'),
    fontUserRelation: require('./fontUserRelationModel'),
    fontUserConfig: require('./fontUserConfigModel')
}
