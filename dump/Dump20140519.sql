CREATE DATABASE  IF NOT EXISTS `skinny_font` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `skinny_font`;
-- MySQL dump 10.13  Distrib 5.6.13, for osx10.6 (i386)
--
-- Host: 127.0.0.1    Database: skinny_font
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Buy_Plan`
--

DROP TABLE IF EXISTS `Buy_Plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Buy_Plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `pv_limit` int(11) NOT NULL,
  `website_limit` int(11) NOT NULL,
  `price` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Buy_Plan`
--

LOCK TABLES `Buy_Plan` WRITE;
/*!40000 ALTER TABLE `Buy_Plan` DISABLE KEYS */;
/*!40000 ALTER TABLE `Buy_Plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Font`
--

DROP TABLE IF EXISTS `Font`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Font` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `font_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `font_type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `font_file_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `font_price` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `font_author` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_popular` tinyint(1) DEFAULT '0',
  `font_en_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `font_url_UNIQUE` (`font_file_name`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Font`
--

LOCK TABLES `Font` WRITE;
/*!40000 ALTER TABLE `Font` DISABLE KEYS */;
INSERT INTO `Font` VALUES (1,'新蒂黑板报','ttf','SantyChalk','12','新蒂',1,'SantyChalk'),(2,'造字工房文研','ttf','MFWenYan_Noncommercial-Regular','6','造字工房',0,'MFWenYan'),(3,'造字工房朗倩','ttf','MFLangQian_Noncommercial-Regular','6','造字工房',0,'MFLangQian'),(4,'造字工房形黑','ttf','MFXingHei_Noncommercial-Light','6','造字工房',1,'MFXingHei'),(5,'造字工房力黑','ttf','MFLiHei_Noncommercial-Regular','6','造字工房',0,'MFLiHei'),(6,'造字工房映画','ttf','MFYingHua_Noncommercial-Regular','6','造字工房',1,'MFYingHua'),(7,'造字工房情书','ttf','MFQingShu_Noncommercial-Regular','6','造字工房',0,'MFQingShu'),(8,'造字工房朗宋','ttf','MFLangSong_Noncommercial-Regular','6','造字工房',0,'MFLangSong'),(9,'造字工房劲黑','ttf','MFJinHei_Noncommercial-Regular','6','造字工房',0,'MFJinHei'),(10,'造字工房雅园','ttf','MFYaYuan_Noncommercial-Regular','6','造字工房',0,'MFYaYuan'),(11,'造字工房版黑','ttf','MFBanHei_Noncommercial-Regular','6','造字工房',0,'MFBanHei'),(12,'造字工房悦黑','ttf','MFYueHei_Noncommercial-Regular','6','造字工房',0,'MFYueHei'),(13,'造字工房尚黑','ttf','MFShangHei_Noncommercial-Regular','6','造字工房',0,'MFShangHei'),(14,'造字工房悦圆','ttf','MFYueYuan_Noncommercial-Regular','6','造字工房',1,'MFYueYuan'),(15,'造字工房俊雅','ttf','MFJunYa_Noncommercial-Regular','6','造字工房',0,'MFJunYa'),(16,'造字工房尚雅','ttf','MFShangYa_Noncommercial-Regular','6','造字工房',0,'MFShangYa'),(17,'造字工房丁丁手绘','ttf','MFDingDing_Noncommercial-Regular','6','造字工房',1,'MFDingDing'),(18,'文泉驿等宽微米黑','ttf','WQYDengKuanWeiMiHei','0','文泉驿',1,'WQYDengKuanWeiMiHei'),(19,'文泉驿等宽正黑','ttf','WQYDengKuanZhengHei','0','文泉驿',0,'WQYDengKuanZhengHei'),(20,'文泉驿微米黑','ttf','WQYWeiMiHei','0','文泉驿',0,'WQYWeiMiHei'),(21,'文泉驿正黑','ttf','WQYZhengHei','0','文泉驿',0,'');
/*!40000 ALTER TABLE `Font` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Font_Order`
--

DROP TABLE IF EXISTS `Font_Order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Font_Order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `font_id` int(11) NOT NULL,
  `buy_plan_id` int(11) NOT NULL,
  `plan_start_time` datetime DEFAULT NULL,
  `plan_end_time` datetime DEFAULT NULL,
  `final_price` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FONT_ORDER_TO_USER_ID_idx` (`user_id`),
  KEY `FONT_ORDER_TO_FONT_ID_idx` (`font_id`),
  KEY `FONT_ORDER_TO_BUY_PLAN_ID_idx` (`buy_plan_id`),
  CONSTRAINT `FONT_ORDER_TO_BUY_PLAN_ID` FOREIGN KEY (`buy_plan_id`) REFERENCES `Buy_Plan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FONT_ORDER_TO_FONT_ID` FOREIGN KEY (`font_id`) REFERENCES `Font` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FONT_ORDER_TO_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Font_Order`
--

LOCK TABLES `Font_Order` WRITE;
/*!40000 ALTER TABLE `Font_Order` DISABLE KEYS */;
/*!40000 ALTER TABLE `Font_Order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Font_User_Config`
--

DROP TABLE IF EXISTS `Font_User_Config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Font_User_Config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `User_id` int(11) NOT NULL,
  `Font_id` int(11) NOT NULL,
  `class` varchar(45) NOT NULL,
  `host` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `User_id` (`User_id`) USING BTREE,
  KEY `Font_id` (`Font_id`) USING BTREE,
  KEY `id` (`id`) USING BTREE,
  CONSTRAINT `FK_User_UserFontConfig` FOREIGN KEY (`User_id`) REFERENCES `User` (`id`),
  CONSTRAINT `FK_Font_UserFontConfig` FOREIGN KEY (`Font_id`) REFERENCES `Font` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Font_User_Config`
--

LOCK TABLES `Font_User_Config` WRITE;
/*!40000 ALTER TABLE `Font_User_Config` DISABLE KEYS */;
INSERT INTO `Font_User_Config` VALUES (37,6,3,'MFLangQian','127.0.0.1'),(38,6,2,'MFWenYan','127.0.0.1'),(39,6,9,'MFJinHei','127.0.0.1');
/*!40000 ALTER TABLE `Font_User_Config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `font_config` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`mail`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'123123@123.com','123456',NULL),(2,'test@gmail.com','333',NULL),(3,'abruzzi.hraig@gmail.com','e10adc3949ba59abbe56e057f20f883e',NULL),(4,'asdf','123',NULL),(5,'asf','912ec803b2ce49e4a541068d495ab570',NULL),(6,'admin','e10adc3949ba59abbe56e057f20f883e',NULL);
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User_Font_Relation`
--

DROP TABLE IF EXISTS `User_Font_Relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User_Font_Relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `User_id` int(11) NOT NULL,
  `Font_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_id_idx` (`User_id`),
  KEY `FK_Font_id_idx` (`Font_id`),
  CONSTRAINT `FK_Font_UserFontRelation` FOREIGN KEY (`Font_id`) REFERENCES `Font` (`id`),
  CONSTRAINT `FK_User_UserFontRelation` FOREIGN KEY (`User_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User_Font_Relation`
--

LOCK TABLES `User_Font_Relation` WRITE;
/*!40000 ALTER TABLE `User_Font_Relation` DISABLE KEYS */;
INSERT INTO `User_Font_Relation` VALUES (1,6,1),(5,6,2),(6,6,3),(7,6,5),(8,6,7),(9,6,9),(10,6,6),(11,6,8),(12,6,15),(13,6,13),(14,6,4),(15,6,10);
/*!40000 ALTER TABLE `User_Font_Relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Website_Config`
--

DROP TABLE IF EXISTS `Website_Config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Website_Config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `website_url` int(11) NOT NULL,
  `custom_class` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `font_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `WEBSITE_CONFIG_TO_USER_ID_idx` (`user_id`),
  KEY `WEBSITE_CONFIT_TO_FONT_ID_idx` (`font_id`),
  CONSTRAINT `WEBSITE_CONFIG_TO_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `WEBSITE_CONFIT_TO_FONT_ID` FOREIGN KEY (`font_id`) REFERENCES `Font` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Website_Config`
--

LOCK TABLES `Website_Config` WRITE;
/*!40000 ALTER TABLE `Website_Config` DISABLE KEYS */;
/*!40000 ALTER TABLE `Website_Config` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-19  1:37:22
